def vowel_swapper(string):
    # ==============
    newString = string.replace('O', '000')
    newString = newString.replace('o', 'ooo')
    newString = newString.replace('A', '4')
    newString = newString.replace('a', '4')
    newString = newString.replace('E', '3')
    newString = newString.replace('e', '3')
    newString = newString.replace('I', '!')
    newString = newString.replace('i', '!')
    newString = newString.replace('U', '|_|')
    newString = newString.replace('u', '|_|')
    return newString
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console